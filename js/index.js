// every time the page loads, load up the saved accessibility from localStorage
let accessibility = window.localStorage.getItem("accessibility")

if (accessibility === null) {
    window.localStorage.setItem("accessibility", "false")
} else if (accessibility === "true") {
    switchAccessibility(true)
    document.querySelector(".accessibility input").checked = true
} else {
    switchAccessibility(false)
    document.querySelector(".accessibility input").checked = false
}

document.querySelector(".accessibility input").addEventListener("change", (e) => {
    switchAccessibility(e.currentTarget.checked)
})

function switchAccessibility(enabled) {
    if (enabled) {
        window.localStorage.setItem("accessibility", "true")
        document.documentElement.style.setProperty('--info-panel-background', '#000000ff')
        document.querySelectorAll(".info-panel").forEach((e) => {
            e.style.width = "430px"
        })
    } else {
        window.localStorage.setItem("accessibility", "false")
        document.documentElement.style.setProperty('--info-panel-background', '#00000033')
        document.querySelectorAll(".info-panel").forEach((e) => {
            e.style.width = "380px"
        })
    }
}