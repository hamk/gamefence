const switchButtons = document.querySelectorAll(".content-switch-buttons-section > .content-switch-buttons > .button")
const switchUnderline = document.querySelector(".button-bottom-border")
const content = document.querySelector(".content-wrapper .content")
const infoContent = document.querySelectorAll(".content-wrapper .content .info > *")
const reviewsContent = document.querySelectorAll(".content-wrapper .content .reviews > *")

const shouldOpenOnReviews = new URLSearchParams(window.location.search).get('open-on') === 'reviews'
console.log(shouldOpenOnReviews)

let currentTimeout = null

let showInfoContent = !shouldOpenOnReviews
switchContent(!shouldOpenOnReviews)

switchButtons[0].addEventListener("click", () => {
    if (showInfoContent) return
    showInfoContent = !showInfoContent
    switchContent(showInfoContent)
})

switchButtons[1].addEventListener("click", () => {
    if (!showInfoContent) return
    showInfoContent = !showInfoContent
    switchContent(showInfoContent)
})

function switchContent(switchInfoContent) {
    window.scrollTo({top: 0, behavior: 'smooth'});
    if (switchInfoContent) {
        switchButtons[0].classList.add("enabled")
        switchButtons[1].classList.remove("enabled")
        switchUnderline.classList.remove("end")
        switchUnderline.classList.add("start")
        content.classList.remove("reviews")
        content.classList.add("info")

        infoContent.forEach((element) => {
            element.style.display = ""
        })

        clearTimeout(currentTimeout)
        currentTimeout = setTimeout(() => {
            reviewsContent.forEach((element) => {
                element.style.display = "none"
            })
        }, 1000)
    } else {
        switchButtons[0].classList.remove("enabled")
        switchButtons[1].classList.add("enabled")
        switchUnderline.classList.remove("start")
        switchUnderline.classList.add("end")
        content.classList.remove("info")
        content.classList.add("reviews")

        reviewsContent.forEach((element) => {
            element.style.display = ""
        })

        clearTimeout(currentTimeout)
        currentTimeout = setTimeout(() => {
            infoContent.forEach((element) => {
                element.style.display = "none"
            })
        }, 1000)
    }
}

// ===== EDIT =====
const editBtns = document.querySelectorAll(".comment__edit")
const deleteBtns = document.querySelectorAll(".comment__delete")
const popup = document.querySelector(".edit-popup-wrapper")
const popupContent = document.querySelector(".edit-popup textarea")
const reviewContainer = document.querySelector(".review-container")
const popupCancelBtn = document.querySelector("#popup-cancel")
const popupUpdateBtn = document.querySelector("#popup-update")
let editingReview = null

editBtns.forEach((editBtn) => {
    editBtn.addEventListener("click", (e) => {
        editingReview = e.currentTarget.parentNode.parentNode
        // fill in the form with the text
        popupContent.value = e.currentTarget.parentNode.parentNode.querySelector('.comment').innerHTML
        popup.classList.remove("hidden")
    })
})

deleteBtns.forEach((deleteBtn) => {
    deleteBtn.addEventListener("click", (e) => {

        const comment_id = e.currentTarget.parentNode.parentNode.getAttribute('data-id')
        console.log(comment_id)

        const xhr = new XMLHttpRequest();

        xhr.open("POST", "comment-handler.php", false);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send('action=delete' +
            '&delete_comment_id=' + encodeURIComponent(comment_id));

        const appendix = new URLSearchParams(window.location.search).get('open-on') === 'reviews' ? '' : '&open-on=reviews'
        window.location.href = window.location.pathname + window.location.search + appendix
    })
})

popupCancelBtn.addEventListener("click", () => {
    popup.classList.add("hidden")
})

popupUpdateBtn.addEventListener("click", () => {
    popup.classList.add("hidden")
    const text = document.querySelector('#edit-comment').value

    const comment_id = editingReview.getAttribute('data-id')
    console.log(text)

    const xhr = new XMLHttpRequest();

    xhr.open("POST", "comment-handler.php", false);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send('action=edit' +
        '&edit_text=' + encodeURIComponent(text) +
        '&edit_comment_id=' + encodeURIComponent(comment_id));

    const appendix = new URLSearchParams(window.location.search).get('open-on') === 'reviews' ? '' : '&open-on=reviews'
    window.location.href = window.location.pathname + window.location.search + appendix
})