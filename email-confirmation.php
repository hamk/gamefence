<?php

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

require_once 'vendor/phpmailer/phpmailer/src/Exception.php';
require_once 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
require_once 'vendor/phpmailer/phpmailer/src/SMTP.php';
require_once 'secrets.php';
require_once 'util.php';


if (!isset($_POST["first-name"])) {
	die("First name not provided.");
}
if (!isset($_POST["last-name"])) {
	die("Last name not provided.");
}
if (!isset($_POST["email"])) {
	die("Email not provided.");
}
if (!isset($_POST["desired-position"])) {
	die("Desired position not provided.");
}
if (!isset($_POST["message"])) {
	die("Message not provided.");
}


// TODO add verification
$applicant_name = htmlentities($_POST["first-name"] . " " . $_POST["last-name"]);
$applicant_address = htmlentities($_POST["email"]);
$applicant_position = htmlentities(strtolower($_POST["desired-position"]));
$applicant_message = htmlentities($_POST["message"]);

// constructing the mail for the applicant
$applicant_mail = new PHPMailer();
$applicant_mail->IsSMTP();
$applicant_mail->Mailer = "smtp";
//$applicant_mail->SMTPDebug  = 1;
$applicant_mail->SMTPAuth = TRUE;
$applicant_mail->SMTPSecure = "tls";
$applicant_mail->Port = 587;
$applicant_mail->Host = "smtp.gmail.com";
$applicant_mail->Username = "notifications.gamefence@gmail.com";
$applicant_mail->Password = GMAIL_PASSWD;

$applicant_mail->IsHTML(true);

// message to be displayed on the site
$message = null;

// actual e-mail content
$applicant_content = "
<p>Hello, $applicant_name, your application has been successfully received! You are applying to be a
<b>$applicant_position</b>. Your message was:</p>
<p>$applicant_message</p>
<p>We will get back to you shortly, please be patient.</p>
<p>Best regards,</p><br/>
<p style='font-size: 1.3em'>GameFENCE</p>
";

$recruiter_content = "
<h2>New application - $applicant_name</h2>
<p>Desired position: $applicant_position</p>
<p>E-mail: $applicant_address</p>
<p>Message: $applicant_message</p>
";

try {
	$applicant_mail->Subject = "Job Application Confirmation";
	$applicant_mail->addAddress($applicant_address, $applicant_name);
	$applicant_mail->setFrom("notifications.gamefence@gmail.com", "GameFENCE Notifications");
	$applicant_mail->msgHTML($applicant_content);
	$applicant_mail->send();

	$applicant_mail->Subject = "Job Application";
    $applicant_mail->clearAddresses();
	$applicant_mail->addAddress("djsushi123@gmail.com", $applicant_name);
	$applicant_mail->AddReplyTo($applicant_address, $applicant_name);
	$applicant_mail->msgHTML($recruiter_content);
	if (!$applicant_mail->send()) {
		$message = "Unfortunately, an error has occurred and the application hasn't been sent successfully.";
	} else {
		$message = "Your application has been sent successfully!";
	}
} catch (Exception $e) {
	// this part of code shouldn't be reached because all validation should be done beforehand
}

open_info_page(
	title: "Email confirmation",
	message: $message,
	button_url: "index.php",
	button_text: "Go back"
);