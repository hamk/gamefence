<?php

function open_info_page(string $title, string $message, string $button_url, string $button_text) {
	header("Location: " . "info-page.php?"
	. "title=$title&"
	. "message=$message&"
	. "button_url=$button_url&"
	. "button_text=$button_text");
}

// our custom timeago function
function timeago($date): string {
	$timestamp = strtotime($date);

	$strTime = array("second", "minute", "hour", "day", "month", "year");
	$length = array("60", "60", "24", "30", "12", "10");

	$diff = time() - $timestamp;
	for ($i = 0; $diff >= $length[$i] && $i < count($length) - 1; $i++) {
		$diff = $diff / $length[$i];
	}

	$diff = round($diff);
	$result = $diff . " " . $strTime[$i];
	if ($diff > 1) $result .= "s";
	$result .= " ago";

	return $result;
}

function require_login() {
	if (session_status() != PHP_SESSION_ACTIVE || !isset($_SESSION['user_uid'])) {
		header('Location: login.php');
	}
}