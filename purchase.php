<?php

require_once 'util.php';
require_once 'connection.php';

session_start();

$conn = connect();

// check if user is logged in
require_login();

$game_uid = $_GET['game_uid'] ?? die("Didn't provide uid.");
$user_uid = $_SESSION['user_uid'];

// check if the user already purchased the game
if ($conn->query("SELECT * FROM purchase WHERE game_uid='$game_uid' AND user_uid='$user_uid'")->num_rows == 1) {
	open_info_page(
		title: "Game already owned",
		message: "You were trying to purchase a game that you already own.",
		button_url: "game-listing.php?uid=$game_uid",
		button_text: "Go back to game"
	);
	exit;
}
$conn->query("INSERT INTO purchase (user_uid, game_uid) VALUES ($user_uid, $game_uid)");
$game = $conn->query("SELECT * FROM game WHERE uid='$game_uid'")->fetch_assoc();

open_info_page(
	title: "Successful purchase",
	message: "You purchased the game {$game['title']}",
	button_url: "game-listing.php?uid=$game_uid",
	button_text: "Go back to game"
);
exit;