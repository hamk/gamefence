<?php
/**
 * This page is a special page. Is receives four parameters by using the GET method:
 *   title - the title of the information page
 *   message - the message that should be displayed on the info page
 *   button_url - the URL where the return button should point to
 *   button_text - the text that has to be displayed on the button
 */

$title = $_GET["title"] ?? die("Didn't provide title.");
$message = $_GET["message"] ?? die ("Didn't provide message.");
$button_url = $_GET["button_url"] ?? die ("Didn't provide button URL.");
$button_text = $_GET["button_text"] ?? die ("Didn't provide button text.");
?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?php echo $title ?></title>

	<link rel="stylesheet" href="css/info-page.css">
</head>
<body>

<span id="title"><?php echo $title ?></span>

<div id="content">
	<span id="message"><?php echo $message ?></span>
	<a href="<?php echo $button_url ?>">
		<div id="back-btn"><?php echo $button_text ?></div>
	</a>
</div>

</body>
</html>