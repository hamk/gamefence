<?php

use JetBrains\PhpStorm\NoReturn;

require_once 'connection.php';
require_once 'util.php';

session_start();

#[NoReturn] function throw400() {
	open_info_page(title: "Error 400", message: "Bad request.", button_url: "index.php", button_text: "Home");
	exit;
}

if (empty($_POST['action'])) throw400();

// handle commenting
$conn = connect();

// ===== HANDLE COMMENT CREATION =====
if ($_POST['action'] == 'create') {
	// validate POST params
	if (empty($_POST['comment']) || empty($_POST['review'])) throw400();
	// default is anonymous user
	$user_uid = 'null';
	if (isset($_SESSION['user_uid'])) {
		$user_uid = "'{$_SESSION['user_uid']}'";
	}

	$query = "INSERT INTO comment (user_uid, game_uid, text)
	VALUES ($user_uid, '{$_GET['uid']}', '{$_POST['review']}')";

	$result = $conn->query($query);

	header("Location: game-listing.php?" . http_build_query($_GET));
	exit;
}

// ===== HANDLE COMMENT EDITING =====
else if ($_POST['action'] == 'edit') {
	if (empty($_POST['edit_text']) || empty($_POST['edit_comment_id'])) exit(400);

	// get the current comment from the database
	$comment = $conn->query("SELECT * FROM comment WHERE uid='{$_POST['edit_comment_id']}' LIMIT 1")->fetch_assoc();

	// require user to be logged in
	if (!isset($_SESSION['user_uid'])) exit(400);

	// the logged-in user has to match the poster of the comment
	if ($_SESSION['user_uid'] != $comment['user_uid']) exit(400);

	$conn->query("UPDATE comment SET text = '{$_POST['edit_text']}' WHERE uid='{$_POST['edit_comment_id']}'");
}

// ===== HANDLE COMMENT DELETIONS =====
else if ($_POST['action'] == 'delete') {
	if (empty($_POST['delete_comment_id'])) exit(400);

	// get the current comment from the database
	$comment = $conn->query("SELECT * FROM comment WHERE uid='{$_POST['delete_comment_id']}' LIMIT 1")->fetch_assoc();

	// require user to be logged in
	if (!isset($_SESSION['user_uid'])) exit(400);

	// the logged-in user has to match the poster of the comment
	if ($_SESSION['user_uid'] != $comment['user_uid']) exit(400);

	$conn->query("DELETE FROM comment WHERE uid='{$_POST['delete_comment_id']}'");
}