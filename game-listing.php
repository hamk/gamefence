<?php
require_once 'connection.php';
require_once 'util.php';
session_start();

if (!isset($_GET["uid"])) {
	open_info_page(
		title: "Error 404",
		message: "Could not find the game you were looking for.",
		button_url: "index.php",
		button_text: "Home"
	);
	exit;
}

$game_uid = $_GET["uid"];

$conn = connect();

$title = "Error";

$result = $conn->query("SELECT * FROM game WHERE uid = '$game_uid'");

if ($result->num_rows === 0) {
	open_info_page(
		title: "Error 404",
		message: "Could not find the game you were looking for.",
		button_url: "index.php",
		button_text: "Home"
	);
	exit;
}
$game = $result->fetch_assoc();

$title = $game["title"];
$genre = $game["genre"];
$short_description = $game["short_description"];
$long_description = $game["long_description"];
$title_image_path = "img/game/" . $game["title_image"];
$release_date = $game["release_date"];
$price = $game['price'];

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <style>
        .heading > .background {
            background: url("<?php echo $title_image_path ?>");
        }
    </style>

    <link rel="stylesheet" href="css/game-listing.css">

    <title><?php echo $title ?></title>
</head>
<body>

<?php require 'header.php' ?>

<div class="edit-popup-wrapper hidden">
    <div class="edit-popup">
        <label for="edit-comment" hidden>New comment</label>
        <textarea id="edit-comment"></textarea>
        <div class="button" id="popup-cancel">Cancel</div>
        <div class="button" id="popup-update">Update</div>
    </div>
</div>

<div class="heading">
    <div class="background"></div>
    <div class="title">
        <div class="left-side">
            <img src="<?php echo $title_image_path ?>" alt="<?php echo $title_image_path ?>">
            <?php
            // check if logged in
            if (empty($_SESSION['user_uid'])) {
	            echo "<div class='price'>{$price}€</div>";
	            echo "<a class='button' href='login.php'><div>Buy</div></a>";
            } else {
	            // check if the user already purchased the game
	            if ($conn->query("SELECT * FROM purchase WHERE game_uid='$game_uid' AND user_uid='{$_SESSION['user_uid']}'")
                        ->num_rows == 1) {
		            echo '<span class="info-text">Already owned.</span>';
	            } else {
                    echo "<div class='price'>$price</div>";
                    echo "<a class='button' href='purchase.php?game_uid=$game_uid'><div>Buy</div></a>";
                }
            }
            ?>
        </div>
        <div class="info">
            <span class="title"><?php echo $title ?></span>
            <span class="genre"><?php echo $genre ?></span>
            <span class="short-description"><?php echo $short_description ?></span>
        </div>
    </div>
</div>

<div class="content-switch-buttons-section">
    <div class="content-switch-buttons">
        <div class="button">Info</div>
        <div class="button">Reviews</div>
        <div class="button-bottom-border-wrapper">
            <div class="button-bottom-border"></div>
        </div>
    </div>
</div>

<div class="content-wrapper">
    <div class="content">
        <div class="info">
            <div class="long-description">
                <span class="title">Description</span>
				<?php
				foreach (explode("\n\r\n", $long_description) as $paragraph) {
					echo "<p>$paragraph</p>";
				}
				?>
            </div>
            <div class="other-info">
                <span class="title">Release date</span>
                <div><?php echo $release_date ?></div>
                <span class="title">Playable on</span>
                <div>
                    <div class="pill">Xbox</div>
                    <div class="pill">PlayStation 4</div>
                    <div class="pill">PC</div>
                </div>
                <span class="title">Capabilities</span>
                <div>
                    <div class="pill">Xbox local co-op (2-4)</div>
                    <div class="pill">Online co-op (2-8)</div>
                    <div class="pill">Xbox local multiplayer (2-4)</div>
                    <div class="pill">Online multiplayer (2-8)</div>
                    <div class="pill">PlayStation multiplayer (2-8)</div>
                    <div class="pill">PlayStation local multiplayer (2-4)</div>
                    <div class="pill">Xbox achievements</div>
                    <div class="pill">Cloud enabled</div>
                    <div class="pill">Single player</div>
                </div>
            </div>
        </div>
        <div class="reviews">
            <div class="review-editor">
                <!--PHP snippet to help preserve the game uid after form submission-->
                <form action="<?php echo "comment-handler.php?" . http_build_query($_GET) ?>" method="post">
                    <textarea title="Review" name="review" id="review" placeholder="Review..."></textarea>
                    <input type="hidden" name="action" value="create">
                    <input class="button" type="submit" value="Publish" name="comment">
                </form>
            </div>
            <div class="review-container">
				<?php
				// review generator
				$result = $conn->query("SELECT * FROM comment WHERE game_uid='$game_uid' ORDER BY creation_timestamp DESC");
				while ($comment = $result->fetch_assoc()) {
					echo "<div class='review' data-id='{$comment['uid']}'><span class='top'>";
					$username = $conn->query("SELECT username FROM user WHERE uid='{$comment['user_uid']}'")
							->fetch_row()[0] ?? "[anonymous]";
                    // TODO add a [delete] to the deleted account comments??
					echo "$username";
					echo " • " . timeago($comment['creation_timestamp']);

					// if the user is logged in
					if (isset($_SESSION['user_uid']) && $_SESSION['user_uid'] == $comment['user_uid']) {
						echo " • " . "<img class='comment__edit' src='img/icon/pencil.png' alt='edit'>";
						echo "<img class='comment__delete' src='img/icon/delete.png' alt='delete'>";
					}
					echo "</span><span class='comment'>";
					echo $comment['text'];
					echo "</span></div>";
				}
				?>
            </div>
        </div>
    </div>
</div>


<script src="js/game-listing.js"></script>
</body>
</html>