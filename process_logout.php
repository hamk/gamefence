<?php

require_once 'util.php';

session_start();

$_SESSION = array();

session_destroy();

open_info_page(
	title: "Success",
	message: "Successfully logged out.",
	button_url: 'index.php',
	button_text: "Home"
);