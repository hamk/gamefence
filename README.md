# GameFENCE

This is a website project for HAMK, the University of Applied Sciences in Hämeenlinna, created by me and my teammate
Masoud.

## TODO List

### Martin

- [x] Add functioning application confirmation page
- [ ] Create the game listing page
- [ ] Create the comment section

### Masoud

- [x] Make a database with all tables
- [ ] Create the user login/register page

### Common

