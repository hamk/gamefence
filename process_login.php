<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <style>
        body {
            background-color: Black;
            color: #A7F7C0;
            font-size: 35px;
            margin-left: 600px;
        }

        h2 {
            margin-left: -50px;
        }

        a {
            margin-left: 150px;
        }
    </style>
</head>

<body>
<?php
require_once 'util.php';
require_once 'connection.php';

$conn = connect();

$username = $_POST['username'];
$password = $_POST['password'];
if (empty($_POST['username'])) {
	die("<h1>Please input username</h1>");
}

$sql = "SELECT * FROM user WHERE username='$username' AND password='$password'";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
	$user_uid = $result->fetch_assoc()["uid"];
	$_SESSION["user_uid"] = $user_uid;
	open_info_page(
		title: "Success",
		message: "You have successfully loggged in with user UID $user_uid!",
		button_url: "index.php",
		button_text: "Go home"
	);
} else {
	open_info_page(
		title: "Login failed",
		message: "Login failed. Incorrect username or password entered.",
		button_url: "login.php",
		button_text: "Go back to login"
	);
}
$conn->close();

?>
</body>

</html>