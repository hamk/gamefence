<?php
require_once 'connection.php';

$conn = connect();

$user_uid = $_SESSION["user_uid"] ?? null;
$username = null;

if ($user_uid != null) {
	$sql = "SELECT username FROM user WHERE uid = '$user_uid'";
	$username = $conn->query($sql)->fetch_row()[0];
}
?>

<?php if ($user_uid != null): ?>
<style>
    .login-register-buttons {
        display: none !important;
    }
</style>
<?php else: ?>
<style>
    .logout-button {
        display: none !important;
    }
</style>
<?php endif ?>

<div class="header">
    <a class="home-button" href="index.php">Gamefence</a>
    <div class="accessibility">
        <label class="switch">
            <input type="checkbox">
            <span class="slider"></span>
        </label>
        <span>Accessibility</span>
    </div>

    <div class="left">
        <div class="logout-button">
            <span class="username"><?php echo $username ?></span>
            <a href="process_logout.php"><div class="button">Logout</div></a>
        </div>
        <div class="login-register-buttons">
            <a href="login.php"><div class="button">Login</div></a>
            <a href="register.php"><div class="button">Register</div></a>
        </div>
    </div>
</div>