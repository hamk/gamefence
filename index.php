<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!--    Main CSS file-->
    <link rel="stylesheet" href="css/main.css">

    <title>GameFENCE</title>
</head>

<body>

<?php
require 'header.php';
require_once 'connection.php';

$conn = connect();
?>

<!-- HERO SECTION - SHELDON: THE TURTLE WAR -->
<div class="hero-section">
    <img src="img/game/sheldon.jpeg" class="background-image-faded" alt="Sheldon"/>
    <div class="top">
        <span class="title">GAMEFENCE STUDIOS</span>
        <span class="subtitle">PRESENTS</span>
    </div>
    <div class="main-game-title">
        <span class="title">SHELDON:</span>
        <br/>
        <span class="subtitle">THE TURTLE WAR</span>
    </div>
</div>

<div class="game-container left">
    <img src="img/game/rogues-end.jpg" class="background-image-faded" alt="Rogue's End"/>
    <div class="info-panel">
        <div class="border">
            <div class="title">ROGUE'S END</div>
            <div class="text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut luctus ante in
                posuere elementum. Aenean congue accumsan mauris eget ultricies. Nam in
                porttitor erat. Nullam non ligula convallis sapien rhoncus ullamcorper. Cras
                ut nibh eros. Aliquam id sem eget justo pharetra iaculis facilisis eget
                massa. Quisque vestibulum porta odio, id vulputate massa fermentum ut. Cras
                magna risus, maximus et elit quis, imperdiet eleifend nisl.
            </div>
        </div>
        <div class="buttons">
            <a href="game-listing.php?uid=3" class="button border">LEARN MORE</a>
            <a class='button' href='purchase.php?game_uid=3'><div>Buy</div></a>
        </div>
    </div>
</div>

<div class="game-container right">
    <img src="img/game/ancient-ruins.jpeg" class="background-image-faded" alt="Ancient Ruins"/>
    <div class="info-panel">
        <div class="border">
            <div class="title">ANCIENT RUINS</div>
            <div class="text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut luctus ante in
                posuere elementum. Aenean congue accumsan mauris eget ultricies. Nam in
                porttitor erat. Nullam non ligula convallis sapien rhoncus ullamcorper. Cras
                ut nibh eros. Aliquam id sem eget justo pharetra iaculis facilisis eget
                massa. Quisque vestibulum porta odio, id vulputate massa fermentum ut. Cras
                magna risus, maximus et elit quis, imperdiet eleifend nisl.
            </div>
        </div>
        <div class="buttons">
            <a href="game-listing.php?uid=2" class="button border">LEARN MORE</a>
            <a class='button' href='purchase.php?game_uid=2'><div>Buy</div></a>
        </div>
    </div>
</div>

<div class="game-container left">
    <img src="img/game/starship.jpg" class="background-image-faded" alt="Starship"/>
    <div class="info-panel">
        <div class="border">
            <div class="title">STARSHIP</div>
            <div class="text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut luctus ante in
                posuere elementum. Aenean congue accumsan mauris eget ultricies. Nam in
                porttitor erat. Nullam non ligula convallis sapien rhoncus ullamcorper. Cras
                ut nibh eros. Aliquam id sem eget justo pharetra iaculis facilisis eget
                massa. Quisque vestibulum porta odio, id vulputate massa fermentum ut. Cras
                magna risus, maximus et elit quis, imperdiet eleifend nisl.
            </div>
        </div>
        <div class="buttons">
            <a href="game-listing.php?uid=4" class="button border">LEARN MORE</a>
            <a class='button' href='purchase.php?game_uid=4'><div>Buy</div></a>
        </div>
    </div>
</div>

<div class="game-container right">
    <img src="img/game/the-journey.jpeg" class="background-image-faded" alt="The Journey"/>
    <div class="info-panel">
        <div class="border">
            <div class="title">THE JOURNEY</div>
            <div class="text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut luctus ante in
                posuere elementum. Aenean congue accumsan mauris eget ultricies. Nam in
                porttitor erat. Nullam non ligula convallis sapien rhoncus ullamcorper. Cras
                ut nibh eros. Aliquam id sem eget justo pharetra iaculis facilisis eget
                massa. Quisque vestibulum porta odio, id vulputate massa fermentum ut. Cras
                magna risus, maximus et elit quis, imperdiet eleifend nisl.
            </div>
        </div>
        <div class="buttons">
            <a href="game-listing.php?uid=5" class="button border">LEARN MORE</a>
            <a class='button' href='purchase.php?game_uid=5'><div>Buy</div></a>
        </div>
    </div>
</div>

<div class="gallery-section">
    <span class="title">OTHER TITLES</span>
    <div id="gallery">
		<?php
		function generate_game(int $uid) {
			global $conn;

			$game = fetch_game($conn, $uid);
			echo "
			<div class='game'>
                <span class='title'>{$game['title']}</span>
                    <a href='game-listing.php?uid=$uid'>
                    <img alt='..' src='img/game/{$game['title_image']}'/>
                </a>
            </div>";
		}

        // selection of the games to show on the main page
        generate_game(6);
        generate_game(7);
        generate_game(8);
        generate_game(9);
        generate_game(10);
        generate_game(11);
        generate_game(12);
        generate_game(13);
        generate_game(14);
        generate_game(15);
		?>
    </div>
</div>

<div class="join-us-section">
    <span class="title">JOIN OUR DEVELOPER TEAM</span>
    <form class="contact-form" method="post" action="email-confirmation.php">
        <input id="first-name" name="first-name" title="First name" type="text" placeholder="First name..." required>
        <input id="last-name" name="last-name" title="Last name" type="text" placeholder="Last name..." required>
        <input id="email" name="email" title="E-mail" type="email" placeholder="E-mail..." required>
        <select id="desired-position" name="desired-position" title="Desired position in team..." required>
            <option value="">Desired position in team...</option>
            <option value="Web developer">Web developer</option>
            <option value="Software engineer">Software engineer</option>
            <option value="Java programmer">Java programmer</option>
        </select>
        <textarea id="message" name="message" title="Message" placeholder="Message..."></textarea>
        <input id="submit" title="Submit" type="submit" value="Submit">
    </form>
</div>

<!--TODO fix mobile responsiveness-->
<div class="footer-section">
    <div class="left-side">
        <span>&copy; 2022 GameFENCE Inc.</span>
        <span>contact@gamefence.org</span>
        <span>Hämeenlinna - Visamäentie 25 - Company of GameFENCE</span>
    </div>
    <div class="right-side">
        <span class="hashtag">#GameFENCE</span>
        <div class="icons">
            <!-- target="_blank" is chosen deliberately to not close this tab so the customer can return anytime -->
            <a href="https://facebook.com" target="_blank">
                <img src="img/icon/facebook.png" alt="Facebook"/>
            </a>
            <a href="https://instagram.com" target="_blank">
                <img src="img/icon/instagram.png" alt="Instagram"/>
            </a>
            <a href="https://linkedin.com" target="_blank">
                <img src="img/icon/linkedin.png" alt="LinkedIn"/>
            </a>
            <a href="https://youtube.com" target="_blank">
                <img src="img/icon/youtube.png" alt="YouTube"/>
            </a>
        </div>
    </div>
</div>

<script src="js/index.js"></script>
</body>
</html>