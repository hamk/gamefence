<?php
require_once 'secrets.php';

function connect(): mysqli {
	try {
		return new mysqli(
			hostname: DB_HOSTNAME,
			username: DB_USERNAME,
			password: DB_PASSWD,
			database: "gamefence",
			port: DB_PORT
		);
	} catch (Exception $e) {
		echo $e;
		die();
	}
}

function fetch_game(mysqli $conn, int $uid): array|false|null {
	return $conn->query("SELECT * FROM game WHERE uid='$uid' LIMIT 1")->fetch_assoc();
}